from flask import Flask, render_template, request, url_for
from get_players2 import *
from pymongo import MongoClient
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)
app.config.update(
	SEND_FILE_MAX_AGE_DEFAULT = 0
	)

client = MongoClient()
db = client.test
names = []
players = db.players.find({})

for player in players:
	if "{} {}".format(player['first_name'], player['last_name']) not in names:
		names.append("{} {}".format(player['first_name'], player['last_name']))

#last_names = db.players.distinct('last_name')
lineups = db.players.distinct('line_up')
event = db.players.distinct('event')
ovr = db.players.distinct('overall')

names.sort()
#last_names.sort()
lineups.sort()
event.sort()
ovr.sort()

@app.route('/', methods=['GET'])
def index():


	# b1 = ["Curry","Stephen","Shooting","Award Winner","90"]
	# b2 =	["Anthony","Carmelo","Two Way","Legends of March","86"]
	# b1_pros, b2_pros, final_score = compare_stats(b1,b2)

	# print b1_pros
	return render_template(
		'index.html',
		names = names,
		lineups = lineups,
		event = event,
		ovr = ovr, 
		)
@app.route("/", methods=['GET','POST'])
def results():
	#stats = ['f_name', 'l_name', 'line', 'even', 'ovrl']
	b1 = []
	b2 = []
	if len(request.form.get('name').split()) == 2:
		b1_fname = request.form.get('name').split()[0]
		b1_lname = request.form.get('name').split()[1]
	else:
		b1_fname = request.form.get('name').split()[0]
		b1_lname = "{} {}".format(request.form.get('name').split()[1],request.form.get('name').split()[2]) 
	b1_lineup = request.form.get('line')
	b1_event = request.form.get('even')
	b1_overall = request.form.get('ovrl')

	b1.append(b1_lname)
	b1.append(b1_fname)
	b1.append(b1_lineup)
	b1.append(b1_event)
	b1.append(b1_overall)


	if len(request.form.get('name2').split()) == 2:
		b2_fname = request.form.get('name2').split()[0]
		b2_lname = request.form.get('name2').split()[1]
	else:
		b2_fname = request.form.get('name2').split()[0]
		b2_lname = "{} {}".format(request.form.get('name2').split()[1],request.form.get('name2').split()[2])
	b2_lineup = request.form.get('line2')
	b2_event = request.form.get('even2')
	b2_overall = request.form.get('ovrl2')
	
	b2.append(b2_lname)
	b2.append(b2_fname)
	b2.append(b2_lineup)
	b2.append(b2_event)
	b2.append(b2_overall)

	b1_pros, b2_pros, b1_pts, b2_pts = compare_stats(b1,b2)

	if b1_pts > b2_pts:
		final_score = b1_pts - b2_pts
	elif b2_pts > b1_pts:
		final_score = b2_pts - b1_pts
	else:
		final_score = 0

	#ballers = db.players.find({})
	return render_template(
		'results.html',
		b1 = "{} {}".format(b1_fname, b1_lname),
		b2 = "{} {}".format(b2_fname, b2_lname),
		b1_pros = b1_pros,
		b2_pros = b2_pros,
		final_score = final_score,
		b1_pts = b1_pts,
		b2_pts = b2_pts
		)