from pymongo import MongoClient
import urllib2
from bs4 import BeautifulSoup
import time

client = MongoClient()
db = client.test

hdr = {'Accept': 'text/html,application/xhtml+xml,*/*',"user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36"}
url = 'http://www.nbalive.gg'

def turn_page(page_num, num):
    
    while True:
        base_url = 'http://www.nbalive.gg/16/players?page=%(0)s' %{"0":page_num}
        req=urllib2.Request(base_url,headers=hdr)
        try:
            html = urllib2.urlopen(req).read()
        except:
            print "nigga"
        else:

            soup = BeautifulSoup(html, 'html.parser')

            tbody = soup.find("tbody")
            atags = tbody.findAll("a")
            #print links

            for atag in atags:
                req=urllib2.Request(url+atag['href'],headers=hdr)
                html = urllib2.urlopen(req).read()
                soup = BeautifulSoup(html, 'html.parser')

                first_name = soup.find("span" ,{'class':'first-name'}).text
                last_name = soup.find("span" ,{'class':'last-name'}).text
                overall = int(soup.find("span" ,{'class':'ovr'}).text)
                line_up = soup.find("dt" ,string='Lineup').find_next_siblings('dd')[0].text
                speed = soup.find("dt" ,string='Speed').find_next_siblings('dd')[0].text
                dribbling = soup.find("dt" ,string='Dribbling').find_next_siblings('dd')[0].text
                pointer_3 = soup.find("dt" ,string='3 Pointer').find_next_siblings('dd')[0].text
                shooting = soup.find("dt" ,string='Shooting').find_next_siblings('dd')[0].text
                defense = soup.find("dt" ,string='Defense').find_next_siblings('dd')[0].text
                passing = soup.find("dt" ,string='Passing').find_next_siblings('dd')[0].text

                second_rates = soup.find("ul", {"class":"secondary-ratings"})

                event = soup.find("ul" ,{'class':'extra-data'}).li.text

                #primary_rates = soup.find("dl", {"class":"primary-ratings"})

               # print second_rates
            ##    for stat in second_rates.find_all('li'):
            ##        str(stat.text)[2:].strip().lower() = int(str(stat.text)[:2].strip())
            ##        #print str(stat.text)[:2].strip(), "=", str(stat.text)[2:].strip()

               # try:
                secondary = {}
                
                for stat in second_rates.find_all('li'):
                    secondary[str(stat.text)[2:].strip().lower()] = int(str(stat.text)[:2].strip())

                result = db.players.insert_one(
                    {
                        "first_name": first_name,
                        "last_name": last_name,
                        "_id": num,
                        "overall" : overall,
                        "line_up": line_up,
                        "event":event,

                        "primary_stats": {

                            "speed": speed,
                            "dribbling":dribbling,
                            "3_pointer":pointer_3,
                            "shooting": shooting,
                            "defense": defense,
                            "passing": passing
                            },

                        "secondary_stats": secondary
                        

                        }
                    )

                print num, "%(0)s %(1)s - %(2)s - Lineup: %(3)s Set: %(4)s" %{"0":first_name,"1":last_name,
                                                                            "2":overall,"3":line_up, "4":event}
                num+=1
            page_num+=1
            turn_page(page_num, num)
#turn_page(1,1)
##                except:
##                    secondary = {}
##                    for stat in second_rates.find_all('li'):
##                        secondary[str(stat.text)[2:].strip().lower()] = int(str(stat.text)[:2].strip())
##                        
##                        db.players.update({"_id": num},{'$set' : {
##                            
##                            "event":event,
##                            "secondary_stats": secondary
##                            }})
##                    print num, "%(0)s %(1)s - %(2)s - Lineup: %(3)s" %{"0":first_name,"1":last_name,
##                                                                       "2":overall,"3":line_up}
##                    
##                    num+=1
##        else:
##            num += 1
##
##def compare_stats(

def compare_stats(p1, p2):
    print p1, p2
    print p1[0], p2[0]
    print p1[1], p2[1]
    print p1[2], p2[2]
    print p1[3], p2[3]
    print p1[4], p2[4]
    baller1 = db.players.find({"last_name":p1[0], "first_name":p1[1],
                               "line_up":p1[2], "event":p1[3], "overall":int(p1[4])})[0]
    baller2 = db.players.find({"last_name":p2[0], "first_name":p2[1],
                               "line_up":p2[2], "event":p2[3], "overall":int(p2[4])})[0]


##    for document in baller1:
##        print document
    main_stats = baller1['primary_stats'].keys()
    second_stats = baller1['secondary_stats'].keys()

    b1_score = 0
    b2_score = 0

    b1_pros = {}
    b2_pros = {}


    results_list = []

    def compare_nums(attribute,stat_cat):
        
        b1_stat = int(baller1[stat_cat][attribute])
        b2_stat = int(baller2[stat_cat][attribute])
        b1_win = b1_stat - b2_stat
        b2_win = b2_stat - b1_stat
        if b1_stat > b2_stat:
            return b1_win, "b1", attribute.title(), b1_stat
            
        elif b2_stat > b1_stat:
            print attribute.title(), "| %(0)s has +%(1)s" %{"0":baller2['last_name'], "1":b2_win}
            return b2_win, "b2", attribute.title(), b2_stat
        else:
            print attribute.title(), "| Both players are equal"
            return -1
            
    for k in main_stats:
        results = compare_nums(k,'primary_stats')
        
        
        if len(str(results)) > 2:
            if results[1] == "b1":
                b1_pros[results[2]] = [results[0],results[3]]
                print k.title(), "| %(0)s has +%(1)s" %{"0":baller1['last_name'], "1":results[0]}
                b1_score+=results[0]
            if results[1] == "b2":
                b2_pros[results[2]] = [results[0],results[3]]
                b2_score+=results[0]
        
       
    for k in second_stats:
        results = compare_nums(k,'secondary_stats')
        if len(str(results)) > 2:
            if results[1] == "b1":
                b1_pros[results[2]] = [results[0],results[3]]
                b1_score+=results[0]
            if results[1] == "b2":
                b2_pros[results[2]] = [results[0],results[3]]
                b2_score+=results[0]

    if b1_score > b2_score:
        print baller1['last_name'], "did better by %(0)s points total!" %{"0":b1_score - b2_score}, baller1['line_up']
        return b1_pros, b2_pros, b1_score, b2_score
        
    else:
        print baller2['last_name'], "did better by %(0)s points total!" %{"0":b2_score - b1_score}, baller2['line_up']
        return b1_pros, b2_pros, b1_score, b2_score
    
##compare_stats(["Pippen '92","Scottie","Defensive","Set Legends","90"],
##              ["Leonard","Kawhi","Defensive","Award Winner","89"])

##compare_stats(["Griffin","Blake","Defensive","Halloween","86"],
##              ["Ibaka","Serge","Defensive","Playoffs","89"])

#compare_stats(["Zach","Randolph '09","Defensive","Throwbacks","91"],
             # ["Ibaka","Serge","Defensive","Playoffs","89"])
