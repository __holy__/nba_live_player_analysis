from flask import Flask, render_template, request, url_for, session, jsonify
from get_playersMongo import *
from insight import *
from pymongo import MongoClient
from flask_bootstrap import Bootstrap
import os
from bson.json_util import dumps
import json
from settings import *


app = Flask(__name__)
Bootstrap(app)
app.config.update(
	SEND_FILE_MAX_AGE_DEFAULT = 0
	)

client = MongoClient(os.environ['MONGO_URI'])
db = client['nbalive']
names_npics = {}
players = db.players.find({})

app.config['SECRET_KEY'] = NBA_SECRET_KEY

for player in players:
	if "{} {}|{}|{}|{}".format(player['first_name'], player['last_name'], player['event'], player['line_up'], player['overall']) not in names_npics:
		names_npics["{} {}|{}|{}|{}".format(player['first_name'], player['last_name'], player['event'], player['line_up'], player['overall'])] = player['image']

#last_names = db.players.distinct('last_name')

names = list(names_npics)
names.sort()
#last_names.sort()


@app.route('/', methods=['GET'])
def index():

	try:
		return render_template(
		'index.html',
		names = session['names'],
		names_npics = sessions['names_npics'],
		)
	except:
		session['names'] = names
		session['names_npics'] = names_npics

	# b1 = ["Curry","Stephen","Shooting","Award Winner","90"]
	# b2 =	["Anthony","Carmelo","Two Way","Legends of March","86"]
	# b1_pros, b2_pros, final_score = compare_stats(b1,b2)

	# print b1_pros
		return render_template(
			'index.html',
			names = session['names'],
			names_npics = session['names_npics'],
			)
@app.route('/',methods=['POST', 'GET'])
def ajaxautocomplete():
    result=''
    
    if request.method=='POST':
    	result = []
    	query=request.form['query']
    	for data in db.players.find({"last_name":{'$regex':r'^{}'.format(query)}}):
    		result.append("{} {}|{}|{}|{}".format(data['first_name'], data['last_name'], data['event'], data['line_up'], data['overall']))
    	
    	result.sort()
    	print result[:3]
 
    	print {"suggestions":result}
    	print jsonify({"suggestions":result})
     	return jsonify({"suggestions":result})

     	print 

    else:
     
     return "ooops"	
@app.route("/results", methods=['GET','POST'])
def results():
	#stats = ['f_name', 'l_name', 'line', 'even', 'ovrl']
	b1 = []
	b2 = []

	b1_pic = db.players.find({})

	splt_up = request.form.get('name').split('|')
	get_event = splt_up[1]
	get_lineup = splt_up[2]
	get_ovrall = splt_up[3]
	get_name = splt_up[0].split()
	#get_name2 = splt_nameNevnt[0].split('')

	if len(get_name) == 2:
		b1_fname = get_name[0]
		b1_lname = get_name[1]
	else:
		try:
			b1_fname = get_name[0]
			b1_lname = "{} {}".format(get_name[1],get_name[2])
		except:
			b1_fname = " "
			b1_lname = get_name[0] 
	b1_lineup = get_lineup
	b1_event = get_event
	b1_overall = get_ovrall

	b1.append(b1_lname)
	b1.append(b1_fname)
	b1.append(b1_lineup)
	b1.append(b1_event)
	b1.append(b1_overall)

	splt_up = request.form.get('name2').split('|')
	get_event = splt_up[1]
	get_lineup = splt_up[2]
	get_ovrall = splt_up[3]
	get_name2 = splt_up[0].split()

	if len(get_name2) == 2:
		b2_fname = get_name2[0]
		b2_lname = get_name2[1]
	else:
		try:
			b2_fname = get_name2[0]
			b2_lname = "{} {}".format(get_name2[1],get_name2[2])
		except:
			b2_fname = " "
			b2_lname = get_name2[0] 
	b2_lineup = get_lineup
	b2_event = get_event
	b2_overall = get_ovrall
	
	b2.append(b2_lname)
	b2.append(b2_fname)
	b2.append(b2_lineup)
	b2.append(b2_event)
	b2.append(b2_overall)

	b1_pros, b2_pros, b1_pts, b2_pts, b1_all, b2_all = compare_stats(b1,b2)

	b1_insi = quick_insights(b1_all)
	b2_insi = quick_insights(b2_all)

	if b1_pts > b2_pts:
		final_score = b1_pts - b2_pts
		result = {}
		for key in b1_insi.keys():
			if key in b2_insi:
				result[key] = b1_insi[key] - b2_insi[key]
	elif b2_pts > b1_pts:
		final_score = b2_pts - b1_pts
		result = {}

		for key in b1_insi.keys():
			if key in b2_insi:
				result[key] = b1_insi[key] - b2_insi[key]
	else:
		final_score = 0
	#ballers = db.players.find({})
	return render_template(
		'results.html',
		result = result,
		b1_info = b1_all,
		b2_info = b2_all,
		b1 = "{} {} ({}) - {}".format(b1_fname, b1_lname, b1_overall, b1_event),
		b2 = "{} {} ({})- {}".format(b2_fname, b2_lname, b2_overall, b2_event),
		b1_pros = b1_pros,
		b2_pros = b2_pros,
		final_score = final_score,
		b1_pts = b1_pts,
		b2_pts = b2_pts,
		b1_insi = b1_insi,
		b2_insi = b2_insi
		)
