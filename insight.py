from pymongo import MongoClient
import urllib2
from bs4 import BeautifulSoup
import time
import os

client = MongoClient('mongodb://holy:Lmfao123@ds155961.mlab.com:55961/nbalive')
db = client['nbalive']

def get_average(attributes):
    avgs = {}

    for attri in attributes:

        stats = map(int, attributes[attri].values())
        avg = sum(stats)/len(stats)
        avgs[attri] = avg
    return avgs
        

        
def quick_insights(player):
    i_offense = {}
    m_offense = {}
    o_offense = {}
    defense = {}
    rebound = {}
    playmaking = {}
    athletics = {}
    inside = {}

    k_stats = [i_offense, m_offense, o_offense, defense,
                rebound, playmaking, athletics, inside]

    main = player['primary_stats']
    latter  = player['secondary_stats']
    for stat in main:
        if "spe" in stat or "drib" in stat:
            athletics[stat] = main[stat]
        elif "3" in stat:
            o_offense[stat] = main[stat]
        elif "shoot" in stat:
            m_offense[stat] = main[stat]
        elif "def" in stat:
            defense[stat] = main[stat]
        else:
            playmaking[stat] = main[stat]

    for stat in latter:
        if "layup" in stat or "dunk" in stat:
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
        if "dunk" in stat or "fight" in stat:
            athletics[stat] = latter[stat]
        if "defensive" in stat or "help" in stat:
            defense[stat] = latter[stat]
        if "post" in stat:
            inside[stat] = latter[stat]
        if "inside" in stat and "likelihood" not in stat:
            inside[stat] = latter[stat]
            i_offense[stat] = latter[stat]
        if "midrange" in stat and "likelihood" not in stat:
            m_offense[stat] = latter[stat]
        if "outside" in stat and "likelihood" not in stat:
            o_offense[stat] = latter[stat]
        if "rebound" in stat:
            rebound[stat]=latter[stat]
        if "touch" in stat:
            o_offense[stat] = latter[stat]
            m_offense[stat] = latter[stat]
        if "scoring" in stat:
            o_offense[stat] = latter[stat]
            m_offense[stat] = latter[stat]
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
            athletics[stat] = latter[stat]
        if "vertical" in stat:
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
            athletics[stat] = latter[stat]
            rebound[stat] = latter[stat]
        if "tena" in stat:
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
            defense[stat] = latter[stat]
            rebound[stat] = latter[stat]
        if "streak" in stat and "shot" not in stat:
            o_offense[stat] = latter[stat]
            m_offense[stat] = latter[stat]
            i_offense[stat] = latter[stat]
        if "pene" in stat:
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
            athletics[stat] = latter[stat]
        if "ball" in stat or "guy" in stat:
            playmaking[stat] = latter[stat]
        if "screens" in stat or "steal" in stat:
            defense[stat] = latter[stat]
        if "pass" or "composure" in stat:
            playmaking[stat] = latter[stat]
        if "block" in stat or "hands" in stat:
            defense[stat] = latter[stat]
        if "first" in stat or "agil" in stat:
            playmaking[stat] = latter[stat]
        if "clutch" in stat:
            o_offense[stat] = latter[stat]
            m_offense[stat] = latter[stat]
            i_offense[stat] = latter[stat]
        if "contact" in stat:
            i_offense[stat] = latter[stat]
            inside[stat] = latter[stat]
            athletics[stat] = latter[stat]
        if "alter" in stat or "inter" in stat:
            defense[stat] = latter[stat]
        if "condi" in stat or "quick" in stat:
            athletics[stat] = latter[stat]
        if "court" in stat or "dribble sp":
            playmaking[stat] = latter[stat]
        if "hedge" in stat:
            defense[stat] = latter[stat]
            athletics[stat] = latter[stat]

        

    ins_stats = {}
    ins_stats['Inside Offense'] = i_offense
    ins_stats['Mid-Range Offense'] = m_offense
    ins_stats['Outside Offense'] = o_offense
    ins_stats['Defense'] = defense
    ins_stats['Rebounding'] = rebound
    ins_stats['Playmaking'] = playmaking
    ins_stats['Athletic Ability'] = athletics
    ins_stats['Inside Effectiveness'] = inside
        
        
    return get_average(ins_stats)
    
        
        
        
        

##    print i_offense
##    print m_offense
##    print o_offense
##    print athletics
##    print playmaking
##quick_insights(db.players.find({})[8])
    
